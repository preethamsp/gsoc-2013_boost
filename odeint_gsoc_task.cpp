#include <iostream>
#include <boost/numeric/odeint.hpp>
#include<boost/numeric/interval.hpp>

using namespace std;
using namespace boost::numeric::odeint;
using namespace boost::numeric::interval_lib;

typedef boost::numeric::interval<double> Interval;

void exponential_decay( const Interval x , Interval &dxdt , const double t )
{
    dxdt = -x;
}
//observer- prints the output in csv format , the first column being time, the second and third being lower and upper bounds of the current state.
void write_cout( const Interval &x , const double t )
{
    cout << t << ","<< x.lower() << "," << x.upper() << endl;
}

int main(){
//declaring the stepper and the initial value of x
    runge_kutta4< Interval,double,Interval,double,vector_space_algebra> stepper;
    Interval x(100,100.1);
    
    integrate_n_steps( stepper , exponential_decay , x , 0.0 , 0.01,500, write_cout );    
    
return 0;}


